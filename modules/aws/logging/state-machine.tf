resource "aws_iam_role" "iam_for_sfn" {
  name        = "iam_for_sfn_logging_exporter"
  description = "IAM role for logging-exporting state machines, brought to you with love from Observability"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Sid    = "",
        Action = "sts:AssumeRole",
        Principal = {
          Service = "states.amazonaws.com"
        }
      }
    ]
  })

  tags = local.tags
}
// Required for
// arn:aws:states:::s3:listObjectsV2
// arn:aws:states:::aws-sdk:s3:copyObject
resource "aws_iam_role_policy" "sfn_policy_s3" {
  name = "s3_access"
  role = aws_iam_role.iam_for_sfn.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "s3:DeleteObject",
          "s3:GetObject",
          "s3:PutObject"
        ],
        Resource = [for b in aws_s3_bucket.logs : "${b.arn}/*"]
      },
      {
        Effect = "Allow",
        Action = [
          "s3:ListBucket"
        ],
        Resource = ["*"]
      }
    ]
  })
}
resource "aws_iam_role_policy" "sfn_policy_lambda" {
  name = "lambda_executor"
  role = aws_iam_role.iam_for_sfn.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "lambda:InvokeFunction"
        ],
        Resource = ["${aws_lambda_function.date_getter.arn}"]
      }
    ]
  })
}
resource "aws_iam_role_policy" "sfn_policy_logs" {
  count = length(local.buckets)
  name  = "log_exporter-${count.index}"
  role  = aws_iam_role.iam_for_sfn.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect : "Allow",
        Action : "logs:DescribeExportTasks", // Does not allow scoped resources
        Resource : "*"
      },
      {
        Effect = "Allow",
        Action = [
          "logs:CreateExportTask"
        ],
        Resource = [
          "${aws_cloudwatch_log_group.groups[count.index].arn}*",
          "${aws_cloudwatch_log_group.groups[count.index].arn}:log-stream:*"
        ]
      }
    ]
  })
}

resource "aws_sfn_state_machine" "sfn-state-machine" {
  count    = length(local.buckets)
  name     = "${local.bucket_purposes[count.index]}-log-archiver" # eg. k8splane-log-archiver
  role_arn = aws_iam_role.iam_for_sfn.arn

  tags = merge(local.tags, {
    LogType = "${local.bucket_purposes[count.index]}"
  })

  definition = jsonencode({
    "Comment" : "Exports logs from cloudwatch, formats for Athena; Manages bucket -> ${local.buckets[count.index]}",
    "StartAt" : "Prepare",
    "States" : {
      "Prepare" : {
        "Type" : "Task",
        "Resource" : "${aws_lambda_function.date_getter.arn}",
        "ResultPath" : "$.Prepare",
        "Next" : "CreateExport"
      },
      "CreateExport" : {
        "Type" : "Task",
        "Resource" : "arn:aws:states:::aws-sdk:cloudwatchlogs:createExportTask",
        "Parameters" : {
          "Destination" : "${local.buckets[count.index]}",
          "From.$" : "$.Prepare.from",
          "LogGroupName" : "${aws_cloudwatch_log_group.groups[count.index].name}",
          "To.$" : "$.Prepare.to"
        },
        "ResultPath" : "$.CreateExport",
        "Next" : "DescribeExport"
      },
      "WaitForExportToSucceedOrFail" : {
        "Type" : "Choice",
        "Choices" : [
          {
            "Variable" : "$.DescribeExport.ExportTasks[0].Status.Code",
            "StringEquals" : "COMPLETED",
            "Next" : "MoveLogFiles"
          },
          {
            "Or" : [
              {
                "Variable" : "$.DescribeExport.ExportTasks[0].Status.Code",
                "StringEquals" : "CANCELLED"
              },
              {
                "Variable" : "$.DescribeExport.ExportTasks[0].Status.Code",
                "StringEquals" : "PENDING_CANCEL"
              }
            ],
            "Next" : "Fail"
          }
        ],
        "Default" : "WaitExport"
      },
      "WaitExport" : {
        "Type" : "Wait",
        "Seconds" : 60,
        "Next" : "DescribeExport"
      },
      "DescribeExport" : {
        "Type" : "Task",
        "Resource" : "arn:aws:states:::aws-sdk:cloudwatchlogs:describeExportTasks",
        "Parameters" : {
          "TaskId.$" : "$.CreateExport.TaskId"
        },
        "ResultPath" : "$.DescribeExport",
        "Next" : "WaitForExportToSucceedOrFail"
      },
      "MoveLogFiles" : {
        "Type" : "Map",
        "Label" : "MoveLogFiles",
        "ItemReader" : {
          "Resource" : "arn:aws:states:::s3:listObjectsV2",
          "Parameters" : {
            "Bucket" : "${local.buckets[count.index]}",
            "Prefix.$" : "States.Format('{}/{}', 'temp', $.CreateExport.TaskId)"
          }
        },
        "ItemSelector" : {
          "destinationBucket" : "${local.buckets[count.index]}",
          "destinationPrefix.$" : "$.Prepare.destinationPrefix",
          "value.$" : "$$.Map.Item.Value"
        },
        "ItemProcessor" : {
          "ProcessorConfig" : {
            "Mode" : "DISTRIBUTED",
            "ExecutionType" : "STANDARD"
          },
          "StartAt" : "CopyObject",
          "States" : {
            "CopyObject" : {
              "Type" : "Task",
              "Resource" : "arn:aws:states:::aws-sdk:s3:copyObject",
              "Parameters" : {
                "Bucket.$" : "$.destinationBucket",
                "CopySource.$" : "States.Format('{}/{}', $.destinationBucket, $.value.Key)",
                "Key.$" : "States.Format('{}/{}-{}', $.destinationPrefix, States.ArrayGetItem(States.StringSplit($.value.Key, '/'), 2), States.ArrayGetItem(States.StringSplit($.value.Key, '/'), 3))"
              },
              "ResultPath" : "$.CopyObject",
              "Next" : "DeleteObject"
            },
            "DeleteObject" : {
              "Type" : "Task",
              "Resource" : "arn:aws:states:::aws-sdk:s3:deleteObject",
              "Parameters" : {
                "Bucket.$" : "$.destinationBucket",
                "Key.$" : "$.value.Key"
              },
              "ResultPath" : "$.DeleteObject",
              "End" : true
            }
          }
        },
        "MaxConcurrency" : 1000,
        "ResultSelector" : {
          "logFilesNum.$" : "States.ArrayLength($)"
        },
        "ResultPath" : "$.MoveLogFiles",
        "Next" : "Succeed"
      },
      "Fail" : {
        "Type" : "Fail",
        "Error" : "ExportTaskError",
        "Cause" : "Export task failed, cancelled, or pending cancellation."
      },
      "Succeed" : {
        "Type" : "Succeed"
      }
    }
    }
  )
}
