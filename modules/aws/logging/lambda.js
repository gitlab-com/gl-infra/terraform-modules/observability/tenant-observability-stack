"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g = Object.create((typeof Iterator === "function" ? Iterator : Object).prototype);
    return g.next = verb(0), g["throw"] = verb(1), g["return"] = verb(2), typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
var getCurrentDate = function (event) {
    if ('currentDate' in event) {
        // For debug : ISO8601 format
        console.debug("Specifed currentDate: ".concat(event['currentDate']));
        return new Date(event['currentDate']);
    }
    return new Date();
};
var getExportTargetDate = function (currentDate) {
    var yesterday = new Date(currentDate.getTime());
    yesterday.setDate(yesterday.getDate() - 1);
    return yesterday;
};
var getExportFromTime = function (lastDate) {
    var fromDate = new Date(lastDate.getTime());
    fromDate.setUTCHours(0);
    fromDate.setUTCMinutes(0);
    fromDate.setUTCSeconds(0);
    fromDate.setUTCMilliseconds(0);
    return fromDate.getTime();
};
var getExportToTime = function (lastDate) {
    var toDate = new Date(lastDate.getTime());
    toDate.setUTCHours(23);
    toDate.setUTCMinutes(59);
    toDate.setUTCSeconds(59);
    toDate.setUTCMilliseconds(999);
    return toDate.getTime();
};
var getDestinationPrefix = function (targetDate) {
    var year = targetDate.getUTCFullYear();
    var month = ('00' + (targetDate.getUTCMonth() + 1)).slice(-2);
    var day = ('00' + (targetDate.getUTCDate())).slice(-2);
    return "".concat(year, "/").concat(month, "/").concat(day);
};
var handler = function (event, context) { return __awaiter(void 0, void 0, void 0, function () {
    var currentDate, exportTargetDate, exportFromTime, exportToTime, destinationPrefix;
    return __generator(this, function (_a) {
        console.debug(event);
        currentDate = getCurrentDate(event);
        exportTargetDate = getExportTargetDate(currentDate);
        exportFromTime = getExportFromTime(exportTargetDate);
        exportToTime = getExportToTime(exportTargetDate);
        console.info("Export from: ".concat(exportFromTime, " - to: ").concat(exportToTime));
        destinationPrefix = getDestinationPrefix(exportTargetDate);
        return [2 /*return*/, {
                destinationPrefix: destinationPrefix,
                from: exportFromTime,
                to: exportToTime,
            }];
    });
}); };
exports.handler = handler;
