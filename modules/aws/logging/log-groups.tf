
resource "aws_cloudwatch_log_group" "groups" {
  count = length(local.bucket_purposes)

  name = "${local.bucket_purposes[count.index]}-logs"
  tags = merge(local.tags, {
    LogType = "${local.bucket_purposes[count.index]}"
  })

  # We only keep logs in cloudwatch log groups for 3 days
  # Ideally this would be 2, but the only values we can use
  # are 0, 1, 3...
  retention_in_days = 3
}

output "log_groups" {
  value = {
    for index, name in local.bucket_purposes :
    name => aws_cloudwatch_log_group.groups[index]
  }
}
