resource "aws_s3_bucket" "logs" {
  count  = length(local.buckets)
  bucket = local.buckets[count.index]

  tags = merge(local.tags, {
    LogType = "${local.bucket_purposes[count.index]}"
  })
}

resource "aws_s3_bucket_public_access_block" "logs" {
  count  = length(local.buckets)
  bucket = aws_s3_bucket.logs[count.index].id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
resource "aws_s3_bucket_policy" "log_bucket_policy" {
  count  = length(local.buckets)
  bucket = aws_s3_bucket.logs[count.index].id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Sid    = "S3BucketReadPermissions",
        Effect = "Allow",
        Principal = {
          // We need to allow cloudwatch access to this bucket
          // but because this isnt an iam role we control,
          // we need to be very careful about where its coming from
          Service = "logs.us-east-1.amazonaws.com"
        },
        Action = [
          // Actions needed for export jobs.
          "s3:GetBucketAcl"
        ],
        Resource = [
          // One policy per bucket.
          aws_s3_bucket.logs[count.index].arn,
        ],
        Condition = {
          StringEquals = {
            // Only allow access from the account we're calling from.
            "aws:SourceAccount" = [
              data.aws_caller_identity.current.account_id
            ]
          },
          ArnLike = {
            // Only from the cloudwatch log group we've configured.
            "aws:SourceArn" = [
              "${aws_cloudwatch_log_group.groups[count.index].arn}:*"
            ]
          }
        }
      },
      {
        Sid    = "S3ObjectWritePermissions",
        Effect = "Allow",
        Principal = {
          // We need to allow cloudwatch access to this bucket
          // but because this isnt an iam role we control,
          // we need to be very careful about where its coming from
          Service = "logs.us-east-1.amazonaws.com"
        },
        Action = [
          // Actions needed for export jobs.
          "s3:PutObject"
        ],
        Resource = [
          // One policy per bucket.
          "${aws_s3_bucket.logs[count.index].arn}/*"
        ],
        Condition = {
          StringEquals = {
            // Only allow access from the account we're calling from.
            "aws:SourceAccount" = [
              data.aws_caller_identity.current.account_id
            ]
          },
          ArnLike = {
            // Only from the cloudwatch log group we've configured.
            "aws:SourceArn" = [
              "${aws_cloudwatch_log_group.groups[count.index].arn}:*"
            ]
          }
        }
      }
    ]
  })
}
resource "aws_s3_bucket_intelligent_tiering_configuration" "logs-tiering" {
  count  = length(local.buckets)
  bucket = aws_s3_bucket.logs[count.index].id
  name   = "EntireBucket"
  status = "Enabled"

  tiering {
    access_tier = "DEEP_ARCHIVE_ACCESS"
    days        = 180
  }
  tiering {
    access_tier = "ARCHIVE_ACCESS"
    days        = 90
  }
}
resource "aws_s3_bucket_lifecycle_configuration" "logs-lifecycle" {
  # For now we can just assign them all the same lifecycle policy, but
  # we can easily change that later!
  count  = length(local.buckets)
  bucket = aws_s3_bucket.logs[count.index].id

  rule {
    id = "rule-1"

    filter {}

    status = "Enabled"

    # Put things in Glacier 12 months after they are written.
    transition {
      days          = 365
      storage_class = "GLACIER"
    }
    # Delete them after 30 months
    expiration {
      days = 913 # 30 months in days, rounded up.
    }
  }
}
