locals {
  # During the renaming of logs, this prefix is the location in the
  # bucket where they go as a temporary location
  temporaryDestinationPrefix = "temp"

  # Map of tags that we attempt to add to everything we create.
  tags = {
    "Role"     = "Observability",
    "TenantID" = var.tenant
  }

  # These are used for tagging purposes.
  # The true name of the bucket is a constrained random string
  # This allows a consistent space for the name, which is constrained to 63 characters max.
  bucket_purposes = ["VPC", "EKS", "RDS", "Other"]
  buckets         = [for s in random_string.bucket_suffixes : "log-archive-${s.result}"]
}

resource "random_string" "bucket_suffixes" {
  count   = length(local.bucket_purposes)
  length  = 16
  special = false
  lower   = true
  upper   = false
}
