resource "aws_iam_role" "iam_for_scheduler" {
  name        = "iam_for_scheduler"
  description = "IAM role for logging-exporting eventbridge, brought to you with love from Observability"
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Sid    = "",
        Action = "sts:AssumeRole",
        Principal = {
          Service = "scheduler.amazonaws.com"
        }
      }
    ]
  })

  tags = local.tags
}

resource "aws_iam_role_policy" "scheduler_policy" {
  name = "state_executor"
  role = aws_iam_role.iam_for_scheduler.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = ["states:StartExecution"],
        Resource = concat(
          [for m in aws_sfn_state_machine.sfn-state-machine : m.arn],
          [for m in aws_sfn_state_machine.sfn-state-machine : "${m.arn}/*"]
        )
      }
    ]
  })
}

resource "aws_scheduler_schedule" "sfn-invoker" {
  count = length(local.buckets)
  name  = "log-exporter-${local.buckets[count.index]}"

  flexible_time_window {
    mode = "OFF"
  }

  schedule_expression = "rate(24 hours)"

  target {
    arn      = aws_sfn_state_machine.sfn-state-machine[count.index].arn
    role_arn = aws_iam_role.iam_for_scheduler.arn
  }
}
