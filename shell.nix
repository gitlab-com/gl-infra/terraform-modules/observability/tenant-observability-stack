# shell.nix
{
  mkShell,
  helm,
  kind,
  coreutils,
  figlet,
  terraform,
  terraform-docs,
  gnumake,
  jsonnet-tool,
  go-jsonnet,
  jsonnet-bundler,
  nodejs,
  localstack,
  typescript,
  awscli
}:
let

in mkShell {
  name = "gl-infra/terraform-modules/observability/observability-cell-stack";
  packages = [
    helm
    coreutils
    kind
    figlet
    terraform
    terraform-docs
    gnumake
    jsonnet-tool
    go-jsonnet
    jsonnet-bundler
    nodejs
    localstack
    typescript
    awscli
  ];
  shellHook = ''
    # So we can spawn multiple shells.
    unset shellHook;
  '';
}
