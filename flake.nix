{
  description = "Observability Cell Stack";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/24.11-beta";
    nixpkgs-terraform.url = "github:stackbuilders/nixpkgs-terraform";
    systems = {
      url = "github:nix-systems/default";
      flake = false;
    };
  };
  nixConfig = {
    extra-substituters = "https://nixpkgs-terraform.cachix.org";
    extra-trusted-public-keys = "nixpkgs-terraform.cachix.org-1:8Sit092rIdAVENA3ZVeH9hzSiqI/jng6JiCrQ1Dmusw=";
  };
  outputs = {self, nixpkgs, nixpkgs-terraform, systems, ...}:
    let
      forEachSystem = nixpkgs.lib.genAttrs (import systems);
    in
    {
      packages = {};
      devShells = forEachSystem
      (system:
        let
          pkgs = import nixpkgs {
            inherit system;
            config.allowUnfree = true;
          };
          terraform = nixpkgs-terraform.packages.${system}."1.6.6";
          # Temporary for testing
          jsonnet-tool = (pkgs.buildGoModule rec {
            pname = "jsonnet-tool";
            version = "1.9.1";
            src = pkgs.fetchFromGitLab {
              owner = "gitlab-com/gl-infra";
              repo  = "jsonnet-tool";
              rev   = "v${version}";
              sha256 = "sha256-zcBK+bxphhS9Fb7xluKIMZyWRLX4QfgzVAazxOo50nw=";
            };
            vendorHash = "sha256-7qdd9VtGPtPlmArWBYXC2eTAAaLM0xAXUxbt7NCyQVU=";
          });
        in
        {
          default = (pkgs.callPackage ./shell.nix {
            mkShell   = pkgs.mkShell;
            helm      = pkgs.kubernetes-helm;
            coreutils = pkgs.coreutils-full;
            kind      = pkgs.kind;
            figlet    = pkgs.figlet;
            terraform = terraform;
            terraform-docs = pkgs.terraform-docs;
            gnumake = pkgs.gnumake;
            jsonnet-tool = jsonnet-tool;
            go-jsonnet = pkgs.go-jsonnet;
            jsonnet-bundler = pkgs.jsonnet-bundler;
            typescript = pkgs.typescript;
            nodejs = pkgs.nodejs_20;
            localstack = pkgs.localstack;
            awscli = pkgs.awscli2;
          });
        });
    };
}
