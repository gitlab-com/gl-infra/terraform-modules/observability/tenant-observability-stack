# Quick Start

## Terraform module

The module is published to a public terraform module registry on GitLab.com:

```terraform
module "tenant_observability_stack" {
  source  = "gitlab.com/gitlab-com/tenant-observability-stack/observability//main"
  version = "1.0.0"

  kube_prometheus_stack_version = "48.6.0"
  grafana_password      = local.grafana_password
  slack_url             = local.slack_url
  pagerduty_service_key = local.pagerduty_service_key

  kube_prometheus_additional_values = []

  cert_exporter_enable_command = true

  redis_address  = local.redis_address
  redis_password = local.redis_password

  tenant_observability_model = local.tenant_observability_model
}
```

## Parameters

| Parameter                         | Type          | Description                                                              | State                         |
|-----------------------------------|---------------|--------------------------------------------------------------------------|-------------------------------|
| tenant_observability_model        | string (YAML) | Sources and flags for observability configuration                        | stable                        |
| namespace                         | string        | Name of kubernetes namespace to use                                      | stable                        |
| create_namespace                  | bool          | Let module create kubernetes namespace                                   | stable                        |
| redis                             | object        | TLS settings for redis                                                   | stable                        |
| redis_address                     | string        | Address of redis instance                                                | to be moved into redis object |
| redis_password                    | string        | Password for redis instance                                              | to be moved into redis object |
| kube_prometheus_stack_version     | string        | Version (without v-prefix) of kube-prometheus-stack helm chart to deploy | to be removed                 |
| kube_prometheus_stack_values      | string        | YAML data for additional values passed to kube-prometheus-stack          | to be removed                 |
| grafana_password                  | string        | Password for Grafana admin account                                       | stable                        |
| slack_url                         | string        | Slack URL environment secret for alerting                                | stable                        |
| pagerduty_service_key             | string        | Pagerduty service key environment secret for alerting                    | stable                        |
| kube_prometheus_additional_values | string        | Additional set values to pass to the kube-prometheus-stack helm chart    | to be removed                 |
