# Development

We use `kind` to boot a local cluster we can deploy this module to.

See below for instructions for both `mise` and `NixOS`.

After deploying the cluster, Prometheus can be accessed through port-forwarding:

```
kubectl port-forward svc/prometheus-operated 9090:9090 -n monitoring
curl http://localhost:9090
```

## Mise-based dependency management

```bash
./scripts/prepare-dev-env.sh

# Start dev environment
source ./scripts/dev-up.sh

# Hack hack hack
terraform plan -var-file=./local.tfvars
terraform apply -var-file=./local.tfvars

# Tear down dev environment
./scripts/dev-down.sh
```

## NixOS

```bash
# Install deps, boot dev environment
nix develop

# Hack hack hack
terraform plan -var-file=./local.tfvars
terraform apply -var-file=./local.tfvars

# Exit shell to trigger teardown behaviour
exit
```

## Container Manifest Generator

`scripts/generate-container-manifest` creates a formatted yaml doc of all container images installed by this module.

When running locally, it expects `GITLAB_TOKEN` to be set; this should be a personal access token that has `read_registry` and `api` privileges, in order to pull the set of jsonnet rules from [Gitlab Dedicated/container-loader](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/container-loader) which are used to template the file

If changes are made to `.gitlab-ci-other-vars.yml`, this script will run automatically via pre-commit and check the updated manifests into code. Additionally, when a new version of this module is released, the manifests will be uploaded to the generic package registry for consumption by downstream users.

## Note on Helm releases

We generally set these options for `helm_release` to get a clean upgrade behavior.
These values have to be set on every `helm_release` resource in this module.

```
atomic = true
cleanup_on_fail = true
reset_values = true
```

## Updating documentation

We use `mkdocs` to generate this documentation.

In order to render this documentation locally, install `mkdocs` and let it serve content to http://127.0.0.1:8000.

```
pip install mkdocs mkdocs-material
cd ./docs
mkdocs serve
```
