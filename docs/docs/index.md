# Overview

## What is the `tenant-observability-stack`?

This terraform module provides common observability tools and services for a single tenant GitLab environment, e.g. for GitLab Dedicated and GitLab Cells.

This module supports tenants in both GCP and AWS and provides the following functionality:

1. [`kube-prometheus-stack`](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack): Prometheus using [Prometheus Operator](https://github.com/prometheus-operator/prometheus-operator) and Grafana on Kubernetes
1. Managed GitLab observability configuration: Prometheus rules, dashboard definitions and [Tamland](https://gitlab.com/gitlab-com/gl-infra/tamland) manifest using the [GitLab metrics catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/reference-architectures/get-hybrid?ref_type=heads)
1. [`redis-exporter`](https://github.com/oliver006/redis_exporter)
1. [`cert-exporter`](https://github.com/joe-elliott/cert-exporter)

![](media/overview.png)

## Guiding principle

The idea behind `tenant-observability-stack` is a strategic one:
We separate observability related aspects from infrastructure projects like Instrumentor and build a module with a well-defined interface to include instead.

This achieves a separation of concerns and allows for organisational decoupling between the Observability Team and other teams using the stack, e.g. the teams around GitLab Dedicated and GitLab Cells.
By separating responsibilities, we can reason about, develop and test the observability stack independently of where it is being used.

## Ongoing work

Work is planned from the overarching epic [&1229: Fundamentals of Tenant Observability](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1229).

## Contact

Reach out to GitLab's [Observability Team](https://handbook.gitlab.com/handbook/engineering/infrastructure/team/scalability/observability/) for any questions, ideally through our [issue tracker](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/).
Internally, you'll find us on Slack in [#g_observability](https://gitlab.enterprise.slack.com/archives/C065RLJB8HK).

![Tenant Observability Stack](media/tenant-observability-stack.png)
