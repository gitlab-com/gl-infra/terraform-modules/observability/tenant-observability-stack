# Observability Configuration

This section details how `config-manager` is used to generate observability configuration like Prometheus rules, Grafana dashboards amongst other types of configuration.

## Overview

The overview below illustrates how [Instrumentor][4] uses the [`tenant-observability-stack`][5] to manage observability configuration.

Configuration is generally sourced from two repositories:

1. [`runbooks`][2]: metrics-catalog (GET reference architecture)
2. [`tenant-observability-config`][3]: Configuration specific for GitLab Dedicated or Cells

The following steps describe how a change to observability configuration is rolled out:

1. Make a change in either of the above source repositories and get a new version released
2. Instrumentor derives the [tenant observability model](#tenant_observability_model) from its tenant model. Change the relevant version in the model by either changing the default version or specifically overriding the version for a tenant only.
3. A terraform run detects that a configuration update is necessary, which triggers a run of [`tenant-observability-config-manager`][1].
4. [`tenant-observability-config-manager`][1] pulls relevant sources from [`runbooks`][2] and [`tenant-observability-config`][3] and generates observability configuration from it.
5. It will install the resulting rules, dashboards, Tamland manifest and other types of configuration as Prometheus resources in the cluster.
6. Outdated configuration resources will be removed as a last step.

![](media/config-manager-flow.png)

The following projects are at play:

* [Instrumentor][4] - an example use case
* [tenant-observability-config-manager][1] - Golang tool to generate configuration in-cluster
* [runbooks][2] - source for GET metrics catalog
* [tenant-observability-config][3] - source for more specific observability configuration
* [tenant-observability-stack][5] - the module at hand

[1]: https://gitlab.com/gitlab-com/gl-infra/observability/tenant-observability/config-manager
[2]: https://gitlab.com/gitlab-com/runbooks/-/tree/master/reference-architectures/get-hybrid?ref_type=heads
[3]: https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/tenant-observability-config
[4]: https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor
[5]: https://gitlab.com/gitlab-com/gl-infra/terraform-modules/observability/tenant-observability-stack

## Forcing an update

`config-manager` is only triggered to run if there was a change to the tenant observability model.
In order to force a run, delete the `kubernetes_job` resource `tenant-observability-config-manager` and apply terraform.

## Tenant Observability Model

The tenant observability model determines how observability configuration for a tenant is generated.
This includes prometheus rules, alerts and Grafana dashboards.

[`config-manager`](https://gitlab.com/gitlab-com/gl-infra/observability/tenant-observability/config-manager) is used to generate this configuration.
Refer to its [README](https://gitlab.com/gitlab-com/gl-infra/observability/tenant-observability/config-manager/-/blob/main/README.md?ref_type=heads#tenant-observability-model) or below section for further information around the observability model.

An example model can look like so (`config-manager` version `v1.8.0`):

```yaml
---
metrics:
    catalog:
        git:
            repo: https://gitlab.com/gitlab-com/runbooks.git
            tag: v3.132.0
    overrides:
        - git:
              repo: https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/tenant-observability-config.git
              tag: v1.6.0
              path: metrics-catalog/gcp
    mixins:
        - source:
              git:
                  repo: https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/tenant-observability-config.git
                  tag: v1.6.0
                  path: metrics-catalog/mixins
          mixins:
              - name: global-dashboards
```

In this example, we pull the metrics-catalog from the runbooks repository at the given tag (`.metrics.catalog`).
The metrics-catalog can be configured through `.metrics.overrides`, which we pull from a separate repository.
This is used to generate metrics, alerts and dashboards from the metrics-catalog.

Mixins provide a mechanism to include more specific configuration.
Multiple sources can be added through `.metrics.mixins`.

For a given mixin source, mixins are expected to live under the specific path `.source.git.path`.
Each mixin will have its own subdirectory with a `mixin.libsonnet` file, which generates the entire mixin.
Further structure can be added to the mixin.

The mixins to enable are specified through the `.mixins` array: In the example above, we only enable the `global-dashboards` mixin.

`config-manager` takes this model, generates observability configuration from it and installs the resources in the cluster.
See [Overview](#overview) above on how this works.

### Using Git sources

A git repository can be used to source configuration, e.g. for mixins.
The config for a git repository has the following parameters:

| Parameter | Type   |                                                            | Description                                              |
|-----------|--------|------------------------------------------------------------|----------------------------------------------------------|
| `repo`    | string | `https://gitlab.com/.../my-repo.git`                       | Source for this repository to pull from                  |
| `tag`     | string | `"v1.6.0"` or `"98168f12ba1466f20966ea526a7fb7fdb8561675"` | A v-prefixed semantic version tag or a long git SHA      |
| `path`    | string | `"subdirectory"`                                           | Optional subdirectory to use, defaults to root directory |
