# `tenant-observability-stack` - Observability stack for single tenant GitLab environments

Please find development documentation for this module here:

https://gitlab-com.gitlab.io/gl-infra/terraform-modules/observability/tenant-observability-stack

![logo][docs/docs/media/tenant-observability-stack.png]

## Data Classification

The resources in this module require and include the data classification label.
If you are adding a new resource to this module, please assess if this label is needed.
See <https://handbook.gitlab.com/handbook/security/data-classification-standard/> for more details.

<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

No providers.

## Modules

No modules.

## Resources

No resources.

## Inputs

No inputs.

## Outputs

No outputs.
<!-- END_TF_DOCS -->
