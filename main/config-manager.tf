resource "kubernetes_service_account_v1" "tenant_observability_config_manager" {
  metadata {
    name      = "tenant-observability-config-manager"
    namespace = var.namespace
  }
}

resource "kubernetes_role_v1" "tenant_observability_config_manager" {
  metadata {
    name      = "tenant-observability-config-manager"
    namespace = var.namespace
  }

  rule {
    api_groups = ["", "monitoring.coreos.com"]
    resources  = ["prometheusrules", "pods", "secrets", "configmaps"]
    verbs      = ["get", "update", "create", "list", "patch", "delete"]
  }
}

resource "kubernetes_role_binding_v1" "tenant_observability_config_manager" {
  metadata {
    name      = "tenant-observability-config-manager"
    namespace = var.namespace
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "tenant-observability-config-manager"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "tenant-observability-config-manager"
    namespace = var.namespace
  }
}

resource "kubernetes_job" "tenant_observability_config_manager" {
  metadata {
    name      = "tenant-observability-config-manager"
    namespace = var.namespace
  }
  wait_for_completion = true
  timeouts {
    create = "5m"
    update = "5m"
  }
  spec {
    backoff_limit = 1
    template {
      metadata {
      }
      spec {
        service_account_name = kubernetes_service_account_v1.tenant_observability_config_manager.metadata[0].name
        container {
          name              = "tenant-observability-config-manager"
          image             = "registry.gitlab.com/gitlab-com/gl-infra/observability/tenant-observability/config-manager:v1.9.3"
          command           = ["/usr/bin/tenant-observability-config-manager"]
          image_pull_policy = "Always"
          args              = ["--namespace=${var.namespace}", "/config/model.yaml"]

          volume_mount {
            mount_path = "/config"
            name       = "config"
          }
          env {
            name  = "GIT_SSH_COMMAND"
            value = "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"
          }
          env {
            # Used to trigger the job if the configuration changed. Not needed by the job itself. Don't depend on it.
            name = "_TENANT_MODEL_SHA_TRIGGER"
            # Include `data_sha1` value for the prom-rules config map to trigger a job run when the included files change.
            value = sha1(yamlencode(kubernetes_config_map.tenant_observability_config_manager_config.data))
          }
        }
        volume {
          name = "config"
          config_map {
            name         = "tenant-observability-config-manager-config"
            default_mode = "0777"
          }
        }
      }
    }
  }
  depends_on = [
    kubernetes_service_account_v1.tenant_observability_config_manager,
    kubernetes_role_v1.tenant_observability_config_manager,
    kubernetes_role_binding_v1.tenant_observability_config_manager
  ]
}

resource "kubernetes_config_map" "tenant_observability_config_manager_config" {
  metadata {
    name      = "tenant-observability-config-manager-config"
    namespace = var.namespace
  }

  data = {
    "model.yaml" = var.tenant_observability_model
  }
}
