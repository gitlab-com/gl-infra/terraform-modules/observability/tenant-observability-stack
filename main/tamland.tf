# Tamland: Cronjob to generate capacity planning forecasts
resource "helm_release" "tamland_forecast" {
  name = "tamland"

  count = var.tamland.enabled ? 1 : 0

  repository = "https://gitlab.com/api/v4/projects/20709159/packages/helm/stable"
  chart      = "tamland"
  version    = "5.4.5"
  namespace  = var.namespace

  values = [
    <<-EOB
    nodeSelector:
      workload: "observability"
    EOB
  ]

  set {
    name  = "forecast_schedule"
    value = "5 6 * * *"
  }

  set {
    name  = "forecast_target"
    value = var.tamland.bucket
  }

  set {
    name  = "second_cache_fs"
    value = "${var.tamland.bucket}/cache"
  }

  set {
    name  = "prometheus_url"
    value = "http://${helm_release.kube_prometheus_stack.chart}-prometheus.${var.namespace}:9090/"
  }

  set {
    name  = "reporting_suspended"
    value = true
  }
}
