variable "namespace" {
  type        = string
  description = "Kubernetes namespace to install resources into"
  nullable    = false
  default     = "monitoring"
}

variable "kube_prometheus_stack_version" {
  type        = string
  description = "Version (without v-prefix) of kube-prometheus-stack helm chart to deploy"
  nullable    = false
}

variable "kube_prometheus_stack_values" {
  type        = string
  description = "YAML data for additional values passed to kube-prometheus-stack"
  sensitive   = false
  nullable    = false
  default     = "{}"
}

variable "grafana_password" {
  type        = string
  description = "Password for Grafana admin account"
  sensitive   = true
  nullable    = false
}

variable "slack_url" {
  type        = string
  description = "Slack URL environment secret for alerting"
  sensitive   = true
  nullable    = false
}

variable "pagerduty_service_key" {
  type        = string
  description = "Pagerduty service key environment secret for alerting"
  sensitive   = true
  nullable    = false
}

variable "kube_prometheus_additional_values" {
  type = list(object({
    name  = string
    value = string
  }))
  description = "Additional set values to pass to the kube-prometheus-stack helm chart"
  nullable    = false
}

variable "redis_address" {
  type        = string
  description = "Address of redis instance"
  nullable    = false
}

variable "redis_password" {
  type        = string
  description = "Password for tenant redis instance"
  sensitive   = true
  nullable    = false
}

# TODO: Move other redis_* variables into this object (kept for compatibility reasons for now)
variable "redis" {
  type = object({
    tls = optional(object({
      enabled = optional(bool, false)
      ca_cert = optional(string)
    }), {})
  })
  description = "Redis-related configuration, e.g. for client connections"
  default     = {}
}

variable "tenant_observability_model" {
  # See https://gitlab.com/gitlab-com/gl-infra/observability/tenant-observability/config-manager for examples
  type        = string
  description = "Describes the desired observability configuration for a tenant (YAML string)"
}

variable "create_namespace" {
  type        = string
  description = "Boolean to determine if this module creates the namespace"
  default     = true
}

variable "cert_exporter_enable_command" {
  type        = bool
  description = "Boolean to determine whether to explicitly define the command for the cert-exporter image"
  default     = true
}

variable "tamland" {
  type = object({
    enabled = bool
    bucket  = optional(string, "")
  })
  description = "Tamland configuration"
  default = {
    enabled = false
  }
}
