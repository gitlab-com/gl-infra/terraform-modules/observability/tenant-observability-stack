resource "helm_release" "kube_prometheus_stack" {
  name = "kube-prometheus-stack"

  repository       = "https://prometheus-community.github.io/helm-charts"
  chart            = "kube-prometheus-stack"
  version          = var.kube_prometheus_stack_version
  namespace        = var.namespace
  create_namespace = var.create_namespace

  atomic          = true
  cleanup_on_fail = true
  reset_values    = true

  values = [var.kube_prometheus_stack_values]

  # Note if you're considering using `set` values here:
  # Unless you have a good reason (eg, passing in a secret or data lookup)
  # Please consider rather applying changes to the values in this
  # helm chart through the `kube-stack-chart-mixins` mechanism
  # This avoids having two ways to configure a single configuration
  # which in turn reduces cognitive load, and makes testing easier.

  set {
    name  = "grafana.adminPassword"
    value = var.grafana_password
  }

  set {
    name  = "alertmanager.config.global.slack_api_url"
    value = var.slack_url
  }

  set {
    name  = "alertmanager.config.receivers[0].pagerduty_configs[0].service_key"
    value = var.pagerduty_service_key
  }

  dynamic "set" {
    for_each = var.kube_prometheus_additional_values
    content {
      name  = set.value.name
      value = set.value.value
    }
  }
}
