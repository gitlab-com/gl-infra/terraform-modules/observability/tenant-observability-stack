resource "kubernetes_secret" "redis_exporter_secrets" {
  metadata {
    name      = "redis-exporter-secrets"
    namespace = var.namespace
    labels = {
      data_classification = "red"
    }
  }

  data = {
    auth_secret = var.redis_password
    tls_ca_cert = var.redis.tls.ca_cert
  }

  depends_on = [
    helm_release.kube_prometheus_stack, # for namespace creation
  ]
}

resource "helm_release" "redis_exporter" {
  name = "redis-exporter"

  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "prometheus-redis-exporter"
  version    = "6.8.0"

  namespace = var.namespace

  atomic          = true
  cleanup_on_fail = true
  reset_values    = true

  set {
    name  = "nodeSelector.workload"
    value = "observability"
  }

  set {
    name  = "tolerations[0].key"
    value = "dedicated/workload"
  }

  set {
    name  = "tolerations[0].value"
    value = "observability"
  }

  set {
    name  = "tolerations[0].operator"
    value = "Equal"
  }

  set {
    name  = "tolerations[0].effect"
    value = "NoSchedule"
  }

  set {
    name  = "redisAddress"
    value = var.redis_address
  }

  set {
    name  = "auth.enabled"
    value = "true"
  }

  set {
    name  = "auth.secret.name"
    value = kubernetes_secret.redis_exporter_secrets.metadata[0].name
  }

  set {
    name  = "auth.secret.key"
    value = "auth_secret"
  }

  set {
    name  = "redisTlsConfig.enabled"
    value = var.redis.tls.enabled
  }
  set {
    name  = "redisTlsConfig.caCertFile.secret.name"
    value = kubernetes_secret.redis_exporter_secrets.metadata[0].name
  }
  set {
    name  = "redisTlsConfig.caCertFile.secret.key"
    value = "tls_ca_cert"
  }
}
