output "monitoring_namespace" {
  value       = var.namespace
  description = "namespace for the tenant monitoring stack"
}

output "prometheus_service_url" {
  value       = "http://${helm_release.kube_prometheus_stack.chart}-prometheus.${var.namespace}:9090/"
  description = "Service URL for the Prometheus service"
}
