#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

cd "$(dirname "${BASH_SOURCE[0]}")/.."

matchContainerManifests='.*\.gitlab-container-manifest.*\.yml'
containerManifests=$(find . -type f -regex "$matchContainerManifests" | sed 's|^\./||')

for manifest in $containerManifests; do
  uploadUrl="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/container-manifests/${CI_COMMIT_TAG}/$manifest"
  echo "Publishing $manifest to $uploadUrl"

  curl --silent --fail-with-body --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "$manifest" "$uploadUrl"
  echo
done
