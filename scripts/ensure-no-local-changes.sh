#!/usr/bin/env bash
# Error out if there are local git changes.

set -euo pipefail
IFS=$'\n\t'

message="${1-found locally changed files.}"

if [[ -n $(git ls-files . --exclude-standard --others | tee /dev/stderr) ]]; then
  echo >&2 "❌ Failure: ${message}"
  exit 1
fi

if ! git diff --exit-code --stat; then
  echo >&2 "❌ Failure: ${message}"
  exit 1
fi

echo >&2 "✅ No local changes detected."
