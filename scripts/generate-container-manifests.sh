#!/usr/bin/env bash

#########################################################################################################################################
# Creates a formatted container manifest file
# from an auto-generated list of images derived from
# the helm chart versions defined in ../.gitlab-ci-other-versions.yml
#
# Formatting rules are defined via jsonnet
# in the container-loader repository:
# https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/container-loader/-/blob/main/jsonnet/container-manifest-rules.jsonnet
#
# This script is designed to be run by renovate and requires CI var GITLAB_CONTAINERLOADER_REGISTRY_TOKEN or GITLAB_TOKEN in order to run
##########################################################################################################################################

set -euo pipefail
cd "$(dirname "${BASH_SOURCE[0]}")/.."

tempImageFile=$(mktemp)

main() {
  mise install go-jsonnet helm
  eval "$(mise activate bash --shims)"

  echo "--------------------------------------"
  echo "Retrieving all images"

  get_images
  formattedImages=$(format_images)

  get_jsonnet_rules
  echo "made it here"
  jsonnet_render "$formattedImages" .gitlab-container-manifest.yml container-manifest-rules.jsonnet

  echo "✅ Manifests created"
  echo "--------------------------------------"

  rm container-manifest-rules.jsonnet
}

get_images() {
  while read -r line; do
    IFS=\# read -r variable metadata <<<"$line"

    name=$(get_dep_name "$metadata")
    version=$(echo "$variable" | cut -d ":" -f2 | tr -d \" | tr -d " ")
    registryUrl=$(get_registry_url "$metadata")

    if [[ $metadata == *"datasource=helm"* ]]; then
      echo "Getting images for chart $name: $version"
      get_helm_images "$name" "$version" "$registryUrl" >>"$tempImageFile"
    elif [[ $metadata == *"datasource=docker"* ]]; then
      echo "Getting docker image $name: $version"
      get_container_image "$name" "$version" "$registryUrl" >>"$tempImageFile"
    fi
  done <.gitlab-ci-other-versions.yml
  echo "✅ Images retrieved"
}

get_helm_images() {
  chartName=$1
  version=$2
  chartURL=$3

  # Check helm is installed
  if ! command -v helm &>/dev/null; then
    echo "Helm is not installed. Please install Helm and try again."
    exit 1
  fi

  helm repo add "$chartName" "$chartURL" >/dev/null

  # Images can be found in charts either from the image: field, or via container args
  # ex: image: "gitlab/gitlab-shell:0.75.0"
  # ex: args:
  #       - --acme-http01-solver-image=quay.io/jetstack/cert-manager-acmesolver:v1.12.12
  imageParser="grep -E '(image:|image=)' | sed -e 's/[ -]*image:[ ]*//' -e 's/\"//g' -e 's/.*image=[ ]*//' | sort -u | tr '\n' ','"

  eval "helm template $chartName/$chartName --version $version | $imageParser"

  helm repo remove "$chartName" >/dev/null
}

get_container_image() {
  imageName=$1
  version=$2
  registryUrl=${3:-''}

  if [[ -z $registryUrl ]]; then
    printf "%s:%s," "$imageName" "$version"
  else
    case "$registryUrl" in
    "https://registry.hub.docker.com/")
      printf "docker.io/%s:%s," "$imageName" "$version"
      ;;
    "https://registry.gitlab.com/")
      printf "registry.gitlab.com/%s:%s," "$imageName" "$version"
      ;;
    "https://quay.io")
      printf "quay.io/%s:%s," "$imageName" "$version"
      ;;
    *)
      exit 1
      ;;
    esac
  fi
}

get_registry_url() {
  metadata="$1"

  for field in $metadata; do
    if [[ $field == "registryUrl"* ]]; then
      echo "$field" | cut -d '=' -f 2
      break
    fi
  done

  echo "" # Return an empty string if no registryUrl is found
}

get_dep_name() {
  metadata="$1"

  for field in $metadata; do
    if [[ $field == "depName"* ]]; then
      echo "$field" | cut -d '=' -f 2
      break
    fi
  done
}

format_images() {
  images=$(cat "$tempImageFile")

  # Split the content into an array using IFS
  IFS=',' read -ra imageArray <<<"$images"

  # Loop through each image
  echo "["
  for image in "${imageArray[@]}"; do
    if [ -n "$image" ]; then
      repository=$(echo "$image" | cut -d ':' -f1)
      tag=$(echo "$image" | cut -d ':' -s -f2-)

      echo "\"$repository:$tag\","
    fi
  done
  echo "]"
}

get_jsonnet_rules() {
  # If running locally, use GITLAB_TOKEN. Otherwise, GITLAB_DEDICATED_CONTAINERLOADER_REGISTRY_TOKEN is set in CI
  if [[ -z ${GITLAB_CONTAINERLOADER_REGISTRY_TOKEN:-} ]]; then
    if [[ -z ${GITLAB_TOKEN:-} ]]; then
      echo "GITLAB_TOKEN does not appear to be set. Please set and try again"
      exit 1
    else
      TOKEN=$GITLAB_TOKEN
    fi
  else
    TOKEN=$GITLAB_CONTAINERLOADER_REGISTRY_TOKEN
  fi

  echo "Retrieving container manifest rules"

  # Note: the main version number is kept in .gitlab-ci-other-versions.yml
  containerLoaderVersion=$(yq .variables.GL_CONTAINER_LOADER_VERSION .gitlab-ci-other-versions.yml)
  containerLoaderID="61910376"
  url="https://gitlab.com/api/v4/projects/$containerLoaderID/packages/generic/container-loader/$containerLoaderVersion/container-manifest-rules.jsonnet"

  manifestRules=$(curl --fail --silent --header "JOB-TOKEN $TOKEN" --output container-manifest-rules.jsonnet "$url" --write-out "%{http_code}")
  if [[ $manifestRules -ne 200 ]]; then
    echo "Failed to retrieve container manifest rules"
    exit 1
  fi
}

jsonnet_render() {
  images=$1
  targetManifest=$2
  targetFIPSManifest=${targetManifest//.yml/-fips.yml}
  manifestRules=$3

  echo "Generating container manifest"
  manifest=$(jsonnet --tla-code images="$images" \
    --tla-code fipsEnabled="false" --string "$manifestRules")
  echo "$manifest" | yq -P >"$targetManifest"

  echo "Generating FIPS container manifest"
  manifest=$(jsonnet --tla-code images="$images" \
    --tla-code fipsEnabled="true" --string "$manifestRules")
  echo "$manifest" | yq -P >"$targetFIPSManifest"
}

main "$@"
