#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)

MODULE_ROOT="$(cd "$SCRIPT_DIR"/.. && pwd)"
RUNTIME_DIR="$MODULE_ROOT/.runtime"
KUBECONFIG="$RUNTIME_DIR/.kube/config"
KUBE_CONFIG_PATH="$KUBECONFIG"
KIND_CLUSTER_NAME="gl-tenant-observability-stack"

# If the .nix-shell directory already exists then just continue as normal, assume
# that this is a second shell and the kind cluster is already provisioned
if [ -d "$RUNTIME_DIR" ]; then
  echo "$RUNTIME_DIR present already, assuming the kind cluster has already been provisioned."
  exit 0
fi

# Just do a quick check to see if weve got access to the docker daemon
# If not then just skip doing anything.
if ! docker ps >/dev/null 2>&1; then
  echo "Docker daemon not found or access missing, skipping shellHook.."
  exit 0
fi

mkdir -p "$RUNTIME_DIR"

# Build a kind cluster
cat <<EOF >"$RUNTIME_DIR"/kind.config
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
name: $KIND_CLUSTER_NAME
nodes:
  - role: control-plane
  - role: worker
    labels:
      workload: support
  - role: worker
    labels:
      workload: observability
EOF

echo "Booting localstack only"
localstack start -d

echo "Spinning up a k8s cluster"
kind create cluster \
  --config "$RUNTIME_DIR/kind.config"

kind export kubeconfig \
  --name "$KIND_CLUSTER_NAME" \
  --kubeconfig "$KUBECONFIG"

echo "Terraform..."
cd main && terraform init

cat <<EOF
Temporary Environment written to $RUNTIME_DIR/env

Useful things to do next:
  - cd main && terraform plan -var-file=./local.tfvars

# Interact with localstack like this
aws --endpoint-url=http://localhost:4566 s3 ls
EOF

cat <<EOF >"$RUNTIME_DIR"/env
export KUBECONFIG=$KUBECONFIG
export KUBE_CONFIG_PATH=$KUBE_CONFIG_PATH
export KIND_CLUSTER_NAME=$KIND_CLUSTER_NAME
export RUNTIME_DIR=$RUNTIME_DIR
export AWS_ACCESS_KEY_ID="test"
export AWS_SECRET_ACCESS_KEY="test"
export AWS_DEFAULT_REGION="us-east-1"
EOF

echo "Sourcing environment from $RUNTIME_DIR/env"
# shellcheck disable=SC1091
source "$RUNTIME_DIR/env"
