#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
MODULE_ROOT="$(cd "$SCRIPT_DIR"/.. && pwd)"
RUNTIME_DIR="$MODULE_ROOT/.runtime"

# If the .runtime directory already exists then just continue as normal, assume
# that this is a second shell and the kind cluster is already provisioned
if [ ! -d "$RUNTIME_DIR" ]; then
  echo "$RUNTIME_DIR not present, exiting."
  exit 0
fi

# shellcheck disable=SC1091
source "$RUNTIME_DIR/env"

# Stop localstack
localstack stop

# Stop kind.
kind delete cluster --name "$KIND_CLUSTER_NAME"

# Remove all the nonsense.
cd "$MODULE_ROOT"
rm -r "$RUNTIME_DIR"
rm -rf .terraform/
rm -f terraform.tfstate terraform.tfstate.backup
rm -f .terraform.lock.hcl
